<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Index extends AbstractController
{
    /**
     * @Route("/" )
     */
    public function index(){
        $apikey="RGAPI-14a664ed-e225-4099-97fd-3eebc8e23b66";
        //On récupére la liste des id des champions gratuits
        $json=file_get_contents("https://euw1.api.riotgames.com/lol/platform/v3/champion-rotations?api_key=".$apikey."");
        $parsed_json=json_decode($json);
        //On récupére la liste des champions pour comparer les deux listes d'id
        $json2=file_get_contents("http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/en_US/champion.json");
        $parsed_json2=json_decode($json2);
        //On récupére l'id des champions gratuit
        $freeChampsId=$parsed_json->freeChampionIds;
        //On récupére tous les id de la liste champion qui sont = à ceux de la liste des champions gratuits
        foreach ($parsed_json2->data as $data)
        {
            foreach ($freeChampsId as $free) {
                if ($data->key == $free) {
                    $freechamps[] = $data->id;
                }
            }

        }

        return $this->render('index/index.html.twig',
        ['freeChamps'=>$freechamps]);
    }


}