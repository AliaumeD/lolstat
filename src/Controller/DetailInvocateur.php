<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DetailInvocateur extends AbstractController
{
    /**
     * @Route("/DetailInvocateur.php",name="detail_invocateur")
     */
    public function DetailInvocateur(){
        $apikey="RGAPI-14a664ed-e225-4099-97fd-3eebc8e23b66";
        //on récupére le pseudo
            $pseudo = $_POST['pseudo'];
                $client = HttpClient::create();
                //On récupére l'id de l'invocateur grace au pseudo tapé par l'utilisateur
                $response = $client->request('GET', "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/".$pseudo."?api_key=".$apikey."");
                //Si le code n'est pas égal à 200 alors on redirige vers une page d'erreur
                if($response->getStatusCode()!=200){

                    return $this->render('erreur/erreur.html.twig');
                }
                else{
                $Data=$response->ToArray();
                $id=$Data['id'];
                // grace à l'id on récupére les info classé d'un invocateur
                $response2 = $client->request('GET', "https://euw1.api.riotgames.com/lol/league/v4/entries/by-summoner/" . $id . "?api_key=" . $apikey . "");
                $InvocateurData=$response2->ToArray();
                //main de l'invocateur via l'api des points de maitrises des invocateurs sur chacun de leur champion joué
                $response3 = $client->request('GET', "https://euw1.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/" . $id . "?api_key=" . $apikey . "");
                $mainData=$response3->ToArray();
                $main = $mainData[0];
                $json=file_get_contents("http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/en_US/champion.json");
                $parsed_json=json_decode($json);
                foreach ($parsed_json->data as $champions)
                {
                    if($champions->key==$main['championId'])
                    {
                        $mainName=$champions->name;
                        $mainId=$champions->id;
                    }
                }
                //On récupére le niveau de l'invocateur
                $lvlInvocateur=$Data['summonerLevel'];
                //On récupére l'id de l'icone de l'invocateur
                $idIcone=$Data['profileIconId'];
                return $this->render('detail_inv/DetailInv.html.twig',
                    ['invocateur' => $InvocateurData,
                        'main' => $main,
                        'mainName'=>$mainName,
                        'lvlInvocateur'=>$lvlInvocateur,
                        'mainId'=>$mainId,
                        'idIcone'=>$idIcone
                    ]);
            }
    }
}