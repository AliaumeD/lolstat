<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;


class ListeChampions extends AbstractController
{
    /**
     * @Route("/ListeChampion",name="champion_list" )
     */

 public function ListeChampion(PaginatorInterface $paginator, Request $request){
    //On récupére le json des champions
     $json=file_get_contents("http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/en_US/champion.json");
     $parsed_json=json_decode($json);
    //On récupére l'id de chacun des champions du json
     foreach ($parsed_json->data as $data)
     {
         $nom[]=$data->id;
     }
    //On crée le paginateur
     $nom = $paginator->paginate(
         $nom,
         $request->query->getInt('page', 1),
         18
     );
 return $this->render('liste/liste.html.twig',
     ['noms'=>$nom]);
 }

    /**
     * @Route("/ListeChampion/{nom}",name="detail_champion")
     */
 public function DetailChampion($nom){
     //On récupére les informations en détail du champion passé en paramètre
     $nom=ucfirst($nom);
     $json=file_get_contents("http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/en_US/champion.json");
     $parsed_json=json_decode($json);
     $data=$parsed_json->data;
     $id=$data->$nom->id;
     $title=$data->$nom->title;
     $blurb=$data->$nom->blurb;
     $attack=$data->$nom->info->attack;
     $defense=$data->$nom->info->defense;
     $magic=$data->$nom->info->magic;
     $difficulty=$data->$nom->info->difficulty;
     return $this->render('detail/DetailChampion.html.twig', [
         'id'=>$id,
         'blurb'=>$blurb,
         'title' => $title,
         'attack' => $attack,
         'defense' => $defense,
         'magic' => $magic,
         'difficulty' => $difficulty

     ]);
 }

}