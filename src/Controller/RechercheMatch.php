<?php


namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheMatch extends AbstractController
{

    /**
     * @Route("/RechercheMatch",name="recherche_match")
     */
    public function recherche_match()
    {
        return $this->render('recherche/recherche_match.html.twig');
    }
}