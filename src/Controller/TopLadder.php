<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TopLadder extends AbstractController
{
    /**
     * @Route("/TopLadder",name="top_ladder" )
     */
    public function Ladder()
    {
        //On récupére la liste des tops joueurs EUW
        $apikey="RGAPI-14a664ed-e225-4099-97fd-3eebc8e23b66";
        $json=file_get_contents("https://euw1.api.riotgames.com/lol/league-exp/v4/entries/RANKED_SOLO_5x5/CHALLENGER/I?page=1&api_key=".$apikey."");
        $parsed_json=json_decode($json);
        return $this->render('topladder/topLadder.html.twig',
            ['tops'=>$parsed_json]);
    }

}