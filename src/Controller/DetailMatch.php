<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DetailMatch extends abstractController
{
    /**
     * @Route("/DetailMatch.php",name="detail_match")
     */
    public function DetailInvocateur()
    {
        $apikey = "RGAPI-4f7b1925-6ab5-4bcb-a72f-6dc96772209c";
        //on récupére le pseudo
        $pseudo = $_POST['pseudo'];
        $client = HttpClient::create();
        //On récupére l'id de l'invocateur grace au pseudo tapé par l'utilisateur
        $response = $client->request('GET', "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/" . $pseudo . "?api_key=" . $apikey . "");
        //Si le code n'est pas égal à 200 alors on redirige vers une page d'erreur
        if ($response->getStatusCode() != 200) {

            return $this->render('erreur/erreur.html.twig');
        } else {
            $Data = $response->ToArray();
            $id = $Data['accountId'];
            $response2 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/" . $id . "?api_key=" . $apikey . "");
            $Data2 = $response2->ToArray();
            $gameId=$Data2['matches'][0]['gameId'];
            $response3 = $client->request('GET', "https://euw1.api.riotgames.com/lol/match/v4/matches/" . $gameId . "?api_key=" . $apikey . "");
            $Data3 = $response3->ToArray();
            foreach ($Data3['teams'] as $listeTeams)
            {
                $teams[]=$listeTeams;
            }

            foreach ($Data3['participants'] as $listeParticipants)
            {
                $participants[]=$listeParticipants;
            }
            foreach ($participants as $stats)
            {
                $statsParticipants[]=$stats['stats'];
            }
            return $this->render('detail_match/detail_match.html.twig',
                ['id' => $id,
                    'data' => $Data3,
                    'teams' => $teams,
                    'participants' => $participants,
                    'statsParticipants' => $statsParticipants,
                    ]);
        }

    }
}