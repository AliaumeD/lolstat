<?php


namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheInv extends AbstractController
{

    /**
     * @Route("/Recherche",name="recherche_invocateur")
     */
    public function recherche_invocateur()
    {
        return $this->render('recherche/recherche.html.twig');
    }
}