#### **Structure Controller/template du site web:**
-ListeChampions.php va récupérer la liste des champions grace au fichier json, de plus une fonction récupére un champion pour afficher ses statistiques en détail dans la template DetaiChampion.html.twig

-Index.php va récupérer la liste des champions gratuits en comparants la clef de chaque champion avec la clef des champions gratuit de la semaine si == alors on la récupére dans une variable et on les affiches dans index.html.twig

-RechercheInv.php renvois juste la template recherche.html.twig qui va renvoyer un formulaire

-DetailInvocateur.php va récupérer les informations d'un invocateur grace au pseudo taper dans la barre de recherche, pour avoir ces information on récupére d'abord l'id de l'invocateur grace au pseudo taper sur l'api de lol puis sur une autre api de lol grace à l'id on récupére toutes les informations d'un invocateur en classé 5*5.

-Enfin TopLadder.php va afficher la liste des tops joueurs du moment du serveur EUW de lol dans la template topLadder.html.twig.

#### **Si erreur mettre votre clef d'API lol dans les variables $apikey des fichiers index.php, DetailInvocateur.php et TopLadder.php, merci**

#### **Image des pages**

**Index avec les champions gratuits**

![Erreur](public/images/index..PNG)

**Détail des champions**

![Erreur](public/images/detail_champion.PNG)

**liste des champions**

![Erreur](public/images/liste_champion.PNG)

**Page de recherche des invocateurs**

![Erreur](public/images/recherche.PNG)

**Détail des invocateurs**

![Erreur](public/images/detail_invoc.PNG)

**Page d'erreur si l'invocateur n'est pas trouvé**

![Erreur](public/images/erreur.PNG)

**Page des tops joueurs EUW**

![Erreur](public/images/chall.PNG)

#### **Si les images ne s'affichent pas vous pouvez directement aller voir dans le fichier public/image/**